import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux"

import store from "./store"

import TimeSheet from "./Components/TimeSheet";


var TIMESHEETS =  [{
  "id": "412946475-2",
  "text": "Marketing Assistant",
  "complete": true,
  "KSR": "17001-BG",
  "Ticket": "284888122-4",
  "ReasonCode": "cursus vestibulum proin eu",
  "Description": "eget tincidunt eget tempus vel pede morbi porttitor",
  "Sat": 2,
  "Sun": 7,
  "Mon": 3,
  "Tue": 2,
  "Wed": 5,
  "Thu": 1,
  "Fri": 6,
  "Total": 3
}, {
  "id": "566435879-3",
  "text": "Operator",
  "complete": false,
  "KSR": "Z-Vacation",
  "Ticket": "393384581-5",
  "ReasonCode": "ut ultrices vel augue vestibulum",
  "Description": "nam nulla integer pede justo",
  "Sat": 6,
  "Sun": 6,
  "Mon": 1,
  "Tue": 7,
  "Wed": 6,
  "Thu": 7,
  "Fri": 6,
  "Total": 5
}, {
  "id": "989808369-7",
  "text": "Assistant Manager",
  "complete": true,
  "KSR": "TA-BA",
  "Ticket": "171513416-8",
  "ReasonCode": "eget elit",
  "Description": "at lorem integer tincidunt ante vel ipsum praesent",
  "Sat": 8,
  "Sun": 4,
  "Mon": 1,
  "Tue": 8,
  "Wed": 4,
  "Thu": 3,
  "Fri": 5,
  "Total": 3
}, {
  "id": "947231008-7",
  "text": "Human Resources Manager",
  "complete": true,
  "KSR": "TA-BA",
  "Ticket": "140549389-5",
  "ReasonCode": "pharetra",
  "Description": "convallis nunc proin at turpis a pede",
  "Sat": 8,
  "Sun": 4,
  "Mon": 1,
  "Tue": 2,
  "Wed": 6,
  "Thu": 2,
  "Fri": 1,
  "Total": 4
}, {
  "id": "915490987-2",
  "text": "Operator",
  "complete": true,
  "KSR": "Z-Vacation",
  "Ticket": "342092626-X",
  "ReasonCode": "odio porttitor id consequat in",
  "Description": "commodo placerat praesent blandit nam nulla integer pede justo",
  "Sat": 5,
  "Sun": 5,
  "Mon": 8,
  "Tue": 6,
  "Wed": 7,
  "Thu": 4,
  "Fri": 8,
  "Total": 7
}]

  const id = Date.now();
  const blanktimesheet = {
  "id": id,
  "text": "r",
  "complete": true,
  "KSR": "",
  "Ticket": "",
  "ReasonCode": "",
  "Description": "",
  "Sat": 0,
  "Sun": 0,
  "Mon": 0,
  "Tue": 0,
  "Wed": 0,
  "Thu": 0,
  "Fri": 0,
  "Total": 0
};
const ksrs = [
  {
    "ksr": "Select KSR",
      "ticketoption": "No",
    "ticket":["2017005512", "2016004466"],
    "reasoncode": ["Select KSR"],
  },{
  "ksr": "TA-BA",
    "ticketoption": "No",
  "ticket":["2017005512", "2016004466"],
  "reasoncode": ["ReasonCodeA", "ReasonCodeB"],
},
{
  "ksr": "TA-SOPS",
  "ticketoption": "Yes",
  "ticket":["2017007777", "2016004466"],
  "reasoncode": ["ReasonCodeA2", "ReasonCodeB2"],

},
{
  "ksr": "TA-SOPSPARTS",
  "ticketoption": "Yes",
    "ticket":["2017005512", "2016004466"],
  "reasoncode": ["ReasonCodeA3", "ReasonCodeB3"],

}
];

//  ksrlisting = {ksrs}

console.log("Begin 001")
ReactDOM.render(

  <Provider store={store}>

  <TimeSheet


   />

 </Provider>
 ,
  document.getElementById('app')
);
