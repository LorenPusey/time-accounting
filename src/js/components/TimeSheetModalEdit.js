import React from "react";

import { Button, Modal, FormGroup, ControlLabel, FormControl, FieldGroup} from 'react-bootstrap';
import TimeSheetEditRowModal from "./TimeSheetEditRowModal";
export default class TimeSheetModalEdit extends React.Component {
  constructor(props) {
    super(props);
    this.closeModalEdit =this.closeModalEdit.bind(this);
    this.updateTimesheetline = this.updateTimesheetline.bind(this)
    this.state = {
      currtimesheet: this.props.timesheets,
      showModalEdit: this.props.showModalEdit,
    };
    // console.log("Constructor TimeSheetModalEdit");
    // console.log(this.state.currtimesheet);
      // console.log(this.props);
      // console.log("Constructor TimeSheetModalEdit END");

  }


  updateTimesheetline() {
    // console.log("CloseEditzzz");
      this.props.updateTimesheetline(this.props.currenttimesheetline.id , this.props.currenttimesheetline)
      this.props.closeModalEdit
    //  this.setState({ showModalEdit: false });
      // console.log("CloseEditaaa");
  }

  closeModalEdit() {
    // console.log("CloseEditzzz");
      this.setState({ showModalEdit: false });
      // console.log("CloseEditaaa");
  }


  render() {
     console.log("TimeSheetModalEdit");
     console.log(this.props);
    var dateList = ["04/07/2017" ,"04/14/2017"  ,"04/21/2017"  ,"04/28/2017"]
    var selectionOptions = [];
      // console.log(this.props)
    dateList.forEach((dateList) => {
  //      console.log(ksrlisting.ksr)
      selectionOptions.push(<option value={dateList} key={dateList}>{dateList}</option>);
  });

    return (
      <Modal  bsSize="large" show={this.props.showModalEdit} onHide={this.close}>
        <Modal.Header>
          <Modal.Title>Edit Time Sheet</Modal.Title>
        </Modal.Header>

        <Modal.Body>
        <TimeSheetEditRowModal
          timesheets={this.props.timesheets}
          currenttimesheetline = {this.props.currenttimesheetline}
          ksrlisting = {this.props.ksrlisting}
          onupdateCurTimesheet={this.props.onupdateCurTimesheet}

         />

        </Modal.Body>

        <Modal.Footer>
          <Button onClick={this.props.closeModalEdit}>Next</Button>
          <Button onClick={this.props.closeModalEdit}>Close</Button>
          <Button onClick={this.props.closeModalEdit} bsStyle="primary">Save changes</Button>
        </Modal.Footer>

      </Modal>

      );

  }
}
