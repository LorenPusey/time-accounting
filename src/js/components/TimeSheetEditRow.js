import React from "react";
import ReactDOM from "react-dom";


export default class TimeSheetEditRow extends React.Component {
  render() {
    return (

    <tr>

      <td><input type="text" name="ksr" onChange={ this.handleChange } /> </td>
      <td><input type="text" name="Ticket" size="6" onChange={ this.handleChange } /></td>
      <td><input type="text" name="ReasonCode" onChange={ this.handleChange } /></td>
      <td><input type="text" name="Description" onChange={ this.handleChange } /></td>

      <td><input type="text" name="Sat" size="1" value = "80"  /></td>
      <td><input type="text" name="Sun" size="1" onChange={ this.handleChange } value = {this.props.blanktimesheet.Sun}/></td>
      <td><input type="text" name="Mon" size="1" onChange={ this.handleChange } value = {this.props.blanktimesheet.Mon} /></td>
      <td><input type="text" name="Tue" size="1" onChange={ this.handleChange } value = {this.props.blanktimesheet.Tue} /></td>
      <td><input type="text" name="Wed" size="1" onChange={ this.handleChange } value = {this.props.blanktimesheet.Wed} /></td>
      <td><input type="text" name="Thu" size="1" onChange={ this.handleChange }  value = {this.props.blanktimesheet.Thu}/></td>
      <td><input type="text" name="Fri" size="1" onChange={ this.handleChange }  value = {this.props.blanktimesheet.Fri}/></td>
      <td><input type="text" name="Total" size="1"  onChange={ this.handleChange } value = {this.props.blanktimesheet.Total} /></td>

      </tr>

      );
  }
}
