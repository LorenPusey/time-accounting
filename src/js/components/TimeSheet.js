import React from "react";
import { connect } from "react-redux"

import TimeSheetHeader from "./TimeSheetHeader";
import TimeSheetTable from "./TimeSheetTable";



import { fetchUserInfo } from "../actions/userActions"
import { fetchTimesheetlines,
          addTimesheetline,
          getBlankTimesheetline,
          deleteTimesheetline,
          submitTimesheetlines,
          updateTimesheetInfoUserName,
          updateTimesheetInfoDate,
          updateTimesheetTotals,
          editTimesheetline,
          getTimesheetline,
          updateTimesheetline,
          updateTimesheetlineItem,
          fetchKSRListing,
         } from "../actions/timesheetsActions"

@connect((store) => {
  return {
    userinfo: store.user.userinfo,
  userFetched: store.user.fetched,
    timesheets: store.timesheet.timesheetlines,
    blanktimesheet: store.timesheet.blanktimesheet,
    currenttimesheetline: store.timesheet.currenttimesheetline,
    currentdate: store.timesheet.currentdate,
    currentuser: store.timesheet.currentuser,
    timesheettotals: store.timesheet.timesheettotals,
    ksrlisting: store.timesheet.ksrlisting,

  };
})



export default class TimeSheet extends React.Component {
  constructor(props) {
    super(props);
    // console.log("Begin constructor")
      // console.log(this.props)
  //      console.log(this.props)


    this.handleFilterTextInput = this.handleFilterTextInput.bind(this);
    this.addNewTimeSheet = this.addNewTimeSheet.bind(this);
    this.openModalEdit = this.openModalEdit.bind(this);
    this.closeModalEdit = this.closeModalEdit.bind(this);
    this.state = {

      showModalEdit: false,
      username : this.props.userinfo.UserFullName,
      date : '',

    };

  }

  componentWillMount() {
     console.log("Begin componentWillMount")
     this.props.dispatch(fetchUserInfo())
     this.props.dispatch(fetchKSRListing())

     var d = new Date();
     console.log((d.getMonth()+1)  + "/" +d.getDate() )
    //  console.log(system.Web.HttpContext.Current.User.Identity.Name.ToString())
     console.log(this.props.userinfo)
     console.log(this.props.blanktimesheet)
     //  this.props.dispatch(fetchTimesheetlines("04/07/2017","CN=Loren Pusey/O=kubota"))
    //    this.props.dispatch(fetchTimesheetlines())
    this.props.dispatch(getBlankTimesheetline() )
    console.log(this.props.blanktimesheet)
    console.log(this.props)
    //  this.props.dispatch(fetchUser())
    //  console.log("componentWillMount Props")
    //        console.log(this.props)
    //    console.log(this.props.timesheetlines)
    //    console.log(this.props.timesheets)
    //console.log(this.state.timesheets)
    console.log("End componentWillMount")
  }

  componentDidMount() {
       console.log("Begin componentDIDMoun Time Sheett")
    // this.props.dispatch(updateTimesheetTotals(this.props.timesheets, this.props.currentdate, this.props.currentuser) )
 }

runupdateTimesheetTotals(){
  console.log("Begin runupdateTimesheetTotals")
 this.props.dispatch(updateTimesheetTotals(this.props.timesheets, this.props.currentdate, this.props.currentuser) )
}

  getTimesheetlines(date, username) {
    //TO-DO need to add Time Sheet save to server here
     console.log("this.state.username" +this.props.currentuser + this.props.userinfo.UserFullName)
    this.props.dispatch(fetchTimesheetlines(date,username))
    this.props.dispatch(updateTimesheetInfoDate(date))
    this.props.dispatch(updateTimesheetInfoUserName(username))


  }

  updateDate(date) {
       console.log(this.props.userinfo)
    this.setState({
      date:date
    });
  }

  submitTimesheetlines() {
    //TO-DO need to add Time Sheet save to server here
    console.log("submitTimesheetlines() ")
    console.log(this.props)
    this.props.dispatch(submitTimesheetlines(this.props.timesheets, this.props.currentdate, this.props.currentuser))

  }

 newFetchUser() {
   // console.log("Begin newFetchUser")
     // console.log(this.props)

     //  this.props.dispatch(fetchTimesheetlines())
     // this.props.dispatch(fetchUser())
       // console.log(this.props)
      // console.log("End newFetchUser")
 }

 handleFilterTextInput(filterText) {
    this.setState({
      filterText: filterText
    });
  }
  getTimesheetline(id){
    console.log("this.props editTimeSheetline1")
     console.log("ID " +id);
     console.log(this.props);
    this.props.dispatch(getTimesheetline(id ))
    console.log("this.props editTimeSheetline2")
     console.log(this.props);
    this.openModalEdit(id)
  //  this.props.dispatch(editTimesheetline(id,  text, this.props.currentdate, this.props.currentuser , this.props.timesheets) )
  }

updateTimesheetlineItem(id, text){
  this.props.dispatch(updateTimesheetlineItem(id,text ))
}
  updateTimesheetline(id, text){
    console.log("this.props updateTimeSheetline1")

       console.log("ID " +id);
       console.log( text);
       console.log(this.props);
       this.runupdateTimesheetTotals()
    this.props.dispatch(updateTimesheetline(id,text ))
    console.log("this.props updateTimeSheetline2")
     console.log(this.props);
    //this.openModalEdit(id)
  //  this.props.dispatch(editTimesheetline(id,  text, this.props.currentdate, this.props.currentuser , this.props.timesheets) )
  }

  addNewTimeSheet(text) {
    const id = Date.now();
    `       console.log("this.props in addNewTimeSheet")`
       console.log(this.props);
        this.props.dispatch(addTimesheetline(id,  text, this.props.currentdate, this.props.currentuser , this.props.timesheets) )
        this.props.dispatch(getBlankTimesheetline() )
      //  this.props.dispatch(updateTimesheetTotals(this.props.timesheets, this.props.currentdate, this.props.currentuser) )

  }

  getBlankTimesheetline(){
      this.props.dispatch(getBlankTimesheetline() )

  }


  deleteTimesheetline(id){
    this.props.dispatch(deleteTimesheetline(id) )
  }

  closeModalEdit() {
    // console.log("CloseEditbbb");
    this.setState({ showModalEdit: false });
  }

  openModalEdit(id) {
      // console.log("OpenModelEditxxx");
   //console.log(component, event);

      // console.log(this.state);
      // console.log(id)
    this.setState({ showModalEdit: true });
  }


  render() {
    const containerStyle = {
      marginTop: "60px"
    };
      const { user, timesheetlines } = this.props;
      //  this.props.dispatch(updateTimesheetTotals(this.props.timesheets, this.props.currentdate, this.props.currentuser) )
    // console.log("Begin")
      // console.log(this.props)
    // console.log(this.props.timesheetlines)
    // console.log(this.props.timesheets)
  //  console.log(this.state.timesheets)
    // console.log("End")
    return (
    //     <button onClick={this.newFetchUser.bind(this)}>FetchUser</button>

      <div>
        <div class="container" style={containerStyle}>
          <div class="row">
            <div class="col-lg-12">
        <TimeSheetHeader
              timesheets={this.props.timesheets}
              onaddNewTimeSheet={this.addNewTimeSheet}
              deleteTimesheetline = {this.deleteTimesheetline}
              getTimesheetlines ={this.getTimesheetlines.bind(this)}
              submitTimesheetlines ={this.submitTimesheetlines.bind(this)}
              blanktimesheet = {this.props.blanktimesheet}
              ksrlisting = {this.props.ksrlisting}
              showModalEdit = {this.state.showModalEdit}
              closeModalEdit = {this.closeModalEdit}
              openModalEdit = {this.openModalEdit}
              username = {this.props.userinfo.UserFullName}
              date = {this.state.date}
              updateDate = {this.updateDate.bind(this)}
                runupdateTimesheetTotals= {this.runupdateTimesheetTotals.bind(this)}
                currenttimesheetline = {this.props.currenttimesheetline}
                getTimesheetline = {this.getTimesheetline.bind(this)}
                updateTimesheetline = {this.updateTimesheetline.bind(this)}
                updateTimesheetlineItem = {this.updateTimesheetlineItem.bind(this)}
                getBlankTimesheetline = {this.getBlankTimesheetline.bind(this)}
                  currentdate = {this.props.currentdate}
        />
        <TimeSheetTable
          timesheets={this.props.timesheets}
          blanktimesheet = {this.props.blanktimesheet}
          showModalEdit = {this.state.showModalEdit}
          closeModalEdit = {this.closeModalEdit}
          openModalEdit = {this.openModalEdit}
          ksrlisting = {this.props.ksrlisting}
          currentdate = {this.props.currentdate}
          deleteTimesheetline = {this.deleteTimesheetline.bind(this)}
          timesheettotals = {this.props.timesheettotals}
          runupdateTimesheetTotals= {this.runupdateTimesheetTotals.bind(this)}
          getTimesheetline = {this.getTimesheetline.bind(this)}
          updateTimesheetline = {this.updateTimesheetline.bind(this)}
          />

          </div>
        </div>
      </div>
    </div>

    );
  }
}
