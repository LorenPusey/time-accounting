import React from "react";
import ReactDOM from "react-dom";

import {Table, Button, Modal, FormGroup, ControlLabel, FormControl, FieldGroup } from 'react-bootstrap';
import TimeSheetEditRowModal from "./TimeSheetEditRowModal";
import TimeSheetModalNew from "./TimeSheetModalNew";
import TimeSheetModalEdit from "./TimeSheetModalEdit";



export default class TimeSheetHeader extends React.Component {
  constructor(props) {
    super(props);
    this.handleFilterTextInputChange = this.handleFilterTextInputChange.bind(this);
    this.addNewTimeSheetAdd = this.addNewTimeSheetAdd.bind(this);
    this.updateCurTimesheet = this.updateCurTimesheet.bind(this);
    this.closeModal =this.closeModal.bind(this);
    this.openModal = this.openModal.bind(this);
    this.closeModalNew =this.closeModalNew.bind(this);
    this.openModalNew = this.openModalNew.bind(this);
      this.handleChange =this.handleChange.bind(this);
  //this.closeModalEdit =this.closeModalEdit.bind(this);
  //  this.openModalEdit = this.openModalEdit.bind(this);
    this.state = {
      showModal: false,
      showModalNew: false,
      showModalEdit: false,
      curtimesheet: this.props.currenttimesheetline,

    };
      // console.log("TimeSheetHeader");
      // console.log(this.props);

  }

  handleFilterTextInputChange(e) {
    this.props.onFilterTextInput(e.target.value);
  }

  getNewTimesheetlines() {
    var username = "CN=Loren Pusey/O=kubota"
    // console.log(this.state)
      this.props.closeModalNew
      // console.log("getNewTimesheetlines")
    this.props.getTimesheetlines(this.state.date, username)


  }

  handleChange(e)  {
    // console.log("handleChange TSH "+ e.target.name + " " + e.target.value)
        //console.log(ksrlisting.ksr)
      //  console.log(e.target.value)
        if (e.target.name === "Date"){
        //  console.log(ksrlisting.reasoncode)
        // console.log(this.state.reasonCodes)
        this.props.updateDate(e.target.value)
        //  this.setState({ date: e.target.value })
        this.props.getTimesheetlines(e.target.value, this.props.username)
        //Run Update
      //  this.props.runupdateTimesheetTotals()


        }


}


  updateCurTimesheet(CurName, CurValue){
    const items = this.props.currenttimesheetline;
    items[CurName] = CurValue;
    // console.log(items)
    //Update Total
    var num =( parseFloat(this.props.currenttimesheetline.Sat) +
      parseFloat(this.props.currenttimesheetline.Sun) +
      parseFloat( this.props.currenttimesheetline.Mon) +
      parseFloat( this.props.currenttimesheetline.Tue) +
      parseFloat( this.props.currenttimesheetline.Wed) +
      parseFloat(this.props.currenttimesheetline.Thu) +
      parseFloat( this.props.currenttimesheetline.Fri) )

        items['Total'] =   num.toString();
         console.log('updateCurTimesheet  ' +  num.toString())
       console.log(items)
    this.props.updateTimesheetline(this.props.currenttimesheetline.id, items );

  }

  addNewTimeSheetAdd() {
      // console.log("TimeSheetHeader  addNewTimeSheetAdd")
      // console.log(this.state);

    const id = Date.now();
    const temp = {
    "id": id,
    "text": "Operator",
    "complete":" true",
    "KSR": "",
    "Ticket": "",
    "ReasonCode": "",
    "Description": "",
    "Sat": "0",
    "Sun": "0",
    "Mon": "0",
    "Tue": "0",
    "Wed": "0",
    "Thu": "0",
    "Fri": "0",
    "Total": "0"
    };
    const tmpsheet = this.state.curtimesheet
    this.setState({ showModal: false });
    //    this.state = {
    //
    //      curtimesheet: temp,
    //    };    this.props.currenttimesheetline
  //  this.props.onaddNewTimeSheet(tmpsheet);
  //    this.setState({curtimesheet: temp });
   this.props.onaddNewTimeSheet(this.props.currenttimesheetline);

  }

  closeModal() {
    this.setState({ showModal: false });
  }

  openModal() {
      // console.log("Openxxx");
      //    console.log(this.state);
      //  this.props.timesheets.forEach((timesheet) => {

    //  });

    this.props.getBlankTimesheetline()
    this.setState({ showModal: true });


  }

    closeModalNew() {
      // console.log("CloseNewbbb");
      this.setState({ showModalNew: false });
    }

  openModalNew() {
        // console.log("OpenNewxxx");
    //    console.log(this.state);
      this.setState({ showModalNew: true });
    }

  render() {
    // console.log("TimeSheetHeader");
    // console.log(this.props);

    var dateList = ["Select","04/07/2017" ,"04/14/2017"  ,"04/21/2017"  ,"04/28/2017"]
    var selectionOptions = [];
  //    console.log(this.props.ksrlisting)
    dateList.forEach((dateList) => {
  //      console.log(ksrlisting.ksr)
      selectionOptions.push(<option value={dateList} key={dateList}>{dateList}</option>);
  });

    return (
      <div>
      <FormGroup controlId="formControlsSelect">
    <ControlLabel>Select Week</ControlLabel>
    <col sm={2}>
    <FormControl
        componentClass="select"
        placeholder="select"
        onChange={ this.handleChange }
        onSelect={ this.handleChange }
        name="Date"
    >
      {selectionOptions}
    </FormControl>
    </col>
  </FormGroup>


      <Button
         bsStyle="primary"
         bsSize="large"
         onClick={this.openModal}
       >
         Add New Line
       </Button>
       <Button
          bsStyle="primary"
          bsSize="large"
          onClick={this.props.submitTimesheetlines}
        >
          Save
        </Button>
        <Button
           bsStyle="primary"
           bsSize="large"
           onClick={this.props.runupdateTimesheetTotals}
         >
           Refresh
         </Button>
      <div className="static-modal">
    <Modal  bsSize="large" show={this.state.showModal} onHide={this.close}>
      <Modal.Header>
        <Modal.Title>Modal title</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <TimeSheetEditRowModal
          currtimesheet = {this.state.curtimesheet}
          ksrlisting = {this.props.ksrlisting}
          onupdateCurTimesheet={this.updateCurTimesheet}
          currenttimesheetline = {this.props.currenttimesheetline}
          updateTimesheetlineItem = {this.props.updateTimesheetlineItem}
         />
      </Modal.Body>

      <Modal.Footer>
        <Button onClick={this.closeModal}>Close</Button>
        <Button onClick={this.addNewTimeSheetAdd} bsStyle="primary">Save changes</Button>
      </Modal.Footer>

    </Modal>
    <TimeSheetModalNew
      showModalNew = {this.state.showModalNew}
      closeModalNew = {this.closeModalNew}
      openModalNew = {this.openModalNew}
      getTimesheetlines ={this.props.getTimesheetlines.bind(this)}
    />
    <TimeSheetModalEdit
        showModalEdit = {this.props.showModalEdit}
        closeModalEdit = {this.props.closeModalEdit}
        ksrlisting = {this.props.ksrlisting}
        onupdateCurTimesheet={this.updateCurTimesheet.bind(this)}
        timesheets={this.props.timesheets}
        currenttimesheetline = {this.props.currenttimesheetline}
        updateTimesheetline = {this.props.updateTimesheetline}
        updateTimesheetlineItem = {this.props.updateTimesheetlineItem}

    />
  </div>
      </div>
    );
  }
}
