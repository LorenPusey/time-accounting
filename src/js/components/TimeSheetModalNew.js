import React from "react";

import { Button, Modal, FormGroup, ControlLabel, FormControl, FieldGroup} from 'react-bootstrap';

export default class TimeSheetModalNew extends React.Component {
  constructor(props) {
    super(props);
    this.closeModalNew =this.closeModalNew.bind(this);
    this.getNewTimesheetlines = this.getNewTimesheetlines.bind(this)
    this.handleChange =this.handleChange.bind(this);
    this.state = {

      showModalNew: this.props.showModalNew,
      date:''
    };

  }

  getNewTimesheetlines() {
    var username = "CN=Loren Pusey/O=kubota"
    console.log(this.state)
      this.props.closeModalNew
      console.log("getNewTimesheetlines")
    this.props.getTimesheetlines(this.state.date, username)


  }
  handleChange(e)  {
  //  console.log("handleChange "+ e.target.name + " " + e.target.value)
        //console.log(ksrlisting.ksr)
      //  console.log(e.target.value)
        if (e.target.name === "Date"){
        //  console.log(ksrlisting.reasoncode)
        // console.log(this.state.reasonCodes)
          this.setState({ date: e.target.value })

        }


}

  closeModalNew() {
    console.log("CloseNewzzz");
      this.setState({ showModalNew: false });
      console.log("CloseNewaaa");
  }


  render() {

    var dateList = ["04/07/2017" ,"04/14/2017"  ,"04/21/2017"  ,"04/28/2017"]
    var selectionOptions = [];
  //    console.log(this.props.ksrlisting)
    dateList.forEach((dateList) => {
  //      console.log(ksrlisting.ksr)
      selectionOptions.push(<option value={dateList} key={dateList}>{dateList}</option>);
  });

    return (
      <Modal  bsSize="large" show={this.props.showModalNew} onHide={this.close}>
        <Modal.Header>
          <Modal.Title>Select Time Sheet</Modal.Title>
        </Modal.Header>

        <Modal.Body>
        <FormGroup controlId="formControlsSelect">
      <ControlLabel>KSR</ControlLabel>
      <FormControl
          componentClass="select"
          placeholder="select"
          onChange={ this.handleChange }
          name="Date"
      >
        {selectionOptions}
      </FormControl>
    </FormGroup>

        </Modal.Body>

        <Modal.Footer>
          <Button onClick={this.props.closeModalNew}>Close</Button>
          <Button onClick={this.getNewTimesheetlines} bsStyle="primary">Save changes</Button>
        </Modal.Footer>

      </Modal>

      );

  }
}
