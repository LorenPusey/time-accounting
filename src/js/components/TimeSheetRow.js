import React from "react";
import {Button} from 'react-bootstrap';


export default class TimeSheetRow extends React.Component {
  constructor(props) {
    super(props);
    this.deleteTimesheetline = this.deleteTimesheetline.bind(this)
    this.editTimesheetline = this.editTimesheetline.bind(this)
    this.getTimesheetline = this.getTimesheetline.bind(this)


  }

  deleteTimesheetline(){
//    console.log("deleteTimeSheetLine")
//    console.log(this.props.timesheet.id)
    this.props.deleteTimesheetline(this.props.timesheet.id)
  }

  editTimesheetline(){
    console.log("editTimeSheetLine")
    console.log(this.props.timesheet.id)
    this.props.editTimesheetline(this.props.timesheet.id)
  }


  getTimesheetline(){
    console.log("getTimeSheetLine")
  //  console.log(this.props.timesheet.id)
    console.log(this.props)
    this.props.getTimesheetline(this.props.timesheet.id)
  }

  componentWillMount() {
       console.log("Begin componentWillMount TimeSheetRow")
  //   this.props.dispatch(updateTimesheetTotals(this.props.timesheets, this.props.currentdate, this.props.currentuser) )
  //Run Update
  //this.props.runupdateTimesheetTotals()

  /*
  <Button
     bsStyle="primary"
     bsSize="small"
     onClick={this.props.editTimesheetline}
   >
     Edit
   </Button>
  */
 }

  render() {
    return (
        <tr>
          <td>
          <Button
             bsStyle="primary"
             bsSize="small"
             onClick={this.deleteTimesheetline}
           >
             Delete
           </Button>
           <Button
              bsStyle="primary"
              bsSize="small"
              onClick={this.getTimesheetline}
            >
              Edit
            </Button>

          </td>
          <td>{this.props.timesheet.KSR}</td>
          <td>{this.props.timesheet.Ticket}</td>
          <td>{this.props.timesheet.ReasonCode}</td>
          <td>{this.props.timesheet.Description}</td>
          <td>{this.props.timesheet.Sat}</td>
          <td>{this.props.timesheet.Sun}</td>
          <td>{this.props.timesheet.Mon}</td>
          <td>{this.props.timesheet.Tue}</td>
          <td>{this.props.timesheet.Wed}</td>
          <td>{this.props.timesheet.Thu}</td>
          <td>{this.props.timesheet.Fri}</td>
          <td>{this.props.timesheet.Total}</td>

          </tr>

      );

  }
}
