import React from "react";
import ReactDOM from "react-dom";

import {Table } from 'react-bootstrap';
import TimeSheetEditRow from "./TimeSheetEditRow";
import TimeSheetRow from "./TimeSheetRow";



export default class TimeSheetTable extends React.Component {

  componentWillMount() {
       console.log("Begin componentDIDMountTS Table")
  //   this.props.dispatch(updateTimesheetTotals(this.props.timesheets, this.props.currentdate, this.props.currentuser) )
  //Run Update
  //this.props.runupdateTimesheetTotals()
 }

  render() {
    var rows = [];
    var lastCategory = null;
    console.log("TimeSheetTableX ")
    console.log(this.props)
    this.props.timesheets.forEach((timesheet) => {
      console.log("TimeSheetTableForEach " + {timesheet})
      rows.push(<TimeSheetRow
        showModalEdit = {this.props.showModalEdit}
        closeModalEdit = {this.props.closeModalEdit}
        openModalEdit = {this.props.openModalEdit}
        timesheet={timesheet} key={timesheet.id}
        ksrlisting = {this.props.ksrlisting}
        deleteTimesheetline = {this.props.deleteTimesheetline.bind(this)}
        getTimesheetline = {this.props.getTimesheetline}
        updateTimesheetline = {this.props.updateTimesheetline}

         />);
    });

console.log("TimeSheetTable2" + rows)
//lll
    const d = new Date(this.props.currentdate.Date);
  //      console.log("this.props.currentdate.5 = " +  (d.getMonth()+1)  + "/" +(d.getDate()-2))
    console.log("this.props.currentdate = " + this.props.currentdate.Date )


  //    console.log(this.props.currentdate )
    if (this.props.currentdate.Date ==null || this.props.currentdate.Date =='Select'){
    //    console.log("this.props.currentdate 1= " + this.props.currentdate.Date )

      var dateSat =""
      var dateSun = ""
      var dateMon = ""
      var dateTue =""
      var dateWed = ""
      var dateThu = ""
      var dateFri =""
    }else{
    //    console.log("this.props.currentdate2 = " +  (d.getMonth()+1)  + "/" +(d.getDate()-2))
      var dateSat =   (' ' + (d.getMonth()+1)  + "/" +(d.getDate()-6))
      var dateSun =   (' ' + (d.getMonth()+1)  + "/" +(d.getDate()-5))
      var dateMon =   (' ' + (d.getMonth()+1)  + "/" +(d.getDate()-4))
      var dateTue =   (' ' + (d.getMonth()+1)  + "/" +(d.getDate()-3))
      var dateWed =  (' ' + (d.getMonth()+1)  + "/" +(d.getDate()-2))
      var dateThu =( ' ' + (d.getMonth()+1)  + "/" +( d.getDate()-1))
      var dateFri = ( ' ' + (d.getMonth()+1)  + "/" +( d.getDate()))
    }

    return (

      <div>


        <h1>Time Sheet</h1>


      <div>
      <Table responsive striped bordered condensed hover>
  <thead>
    <tr>
      <th>Edit</th>
      <th>KSR/TA #</th>
      <th>Ticket/SOR Number</th>
      <th>Reason Code</th>
      <th>Description</th>
      <th>Sat{dateSat}</th>
      <th>Sun{dateSun}</th>
      <th>Mon{dateMon}</th>
      <th>Tue{dateTue}</th>
      <th>Wed {dateWed}</th>
      <th>Thu{dateThu}</th>
      <th>Fri{dateFri}</th>
      <th>Total</th>
    </tr>
    <tr>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th>Totals</th>
      <th>{this.props.timesheettotals.Sat}</th>
      <th>{this.props.timesheettotals.Sun}</th>
      <th>{this.props.timesheettotals.Mon}</th>
      <th>{this.props.timesheettotals.Tue}</th>
      <th>{this.props.timesheettotals.Wed}</th>
      <th>{this.props.timesheettotals.Thu}</th>
      <th>{this.props.timesheettotals.Fri}</th>
      <th>{this.props.timesheettotals.Total}</th>
    </tr>
  </thead>
  <tbody>

        {rows}
        </tbody>
      </Table>
      </div>
      </div>
    );
  }
}
