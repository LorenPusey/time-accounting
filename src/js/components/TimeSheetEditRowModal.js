import React from "react";
import ReactDOM from "react-dom";

import {Table, Button, Modal, FormGroup, ControlLabel, FormControl, FieldGroup } from 'react-bootstrap';


export default class TimeSheetEditRowModal extends React.Component {
  constructor(props) {
    super(props);
    //  const tmpsheet =  this.props.blanktimesheet
    const id = Date.now();
    var tmpsheet =    {
      "id": id  ,
      "text":  ""  ,
      "complete":  ""  ,
      "KSR": "" ,
      "Ticket":  "" ,
      "ReasonCode":  ""  ,
      "Description":  ""  ,
      "Sat": 0  ,
      "Sun": 0  ,
      "Mon":  0  ,
      "Tue":  0  ,
      "Wed":  0  ,
      "Thu":0  ,
      "Fri": 0  ,
      "Total": 0  ,
    }
    //console.log("tmpSheet")
    //console.log(tmpsheet)
    this.state = {
      curtimesheet: this.props.currenttimesheetline,
      reasonCodes:[],
      ticketoption:'No',
      tickets:[],
      ksrval: '',

    };
    this.handleChange =this.handleChange.bind(this);

}

componentWillMount() {
  const id = Date.now();
  var tmpsheet =    {
    "id": id  ,
    "text":  ""  ,
    "complete":  ""  ,
    "KSR": "" ,
    "Ticket":  "" ,
    "ReasonCode":  ""  ,
    "Description":  ""  ,
    "Sat": 0  ,
    "Sun": 0  ,
    "Mon":  0  ,
    "Tue":  0  ,
    "Wed":  0  ,
    "Thu":0  ,
    "Fri": 0  ,
    "Total": 0  ,
  }
  this.setState({
     curtimesheet: tmpsheet
   });

}

handleChange(e)  {
    console.log("handleChange "+ e.target.name + " " + e.target.value)
  this.props.onupdateCurTimesheet( e.target.name, e.target.value)

  // Get Reason code
  this.props.ksrlisting.forEach((ksrlisting) => {
    //console.log(ksrlisting.ksr)
    //  console.log(e.target.value)
      if (e.target.value === ksrlisting.ksr){
      //  console.log(ksrlisting.reasoncode)
      // console.log(this.state.reasonCodes)
        this.setState({ reasonCodes: ksrlisting.reasoncode })
        this.setState({ tickets:  ksrlisting.ticket })
        this.setState({ ticketoption:  ksrlisting.ticketoption})


      }


  });

  //console.log(this.state.reasonCodes)
  //console.log("handleChange  END"+ e.target.name + " " + e.target.value)

};



render() {
    //console.log("TimeSheetEditRowModal");
    //console.log(this.props);
    var ksroptions = [];
//    console.log(this.props.ksrlisting)
    this.props.ksrlisting.forEach((ksrlisting) => {
    //  console.log("loadinggg");
    //  console.log(ksrlisting.ksr)
      ksroptions.push(<option value={ksrlisting.ksr} key={ksrlisting.ksr}>{ksrlisting.ksr}</option>);
      //Check if Ticket needed

    });

  var reasoncodeoptions = ['<option value="Select" key="Select" >"Select" </option>'];
  this.state.reasonCodes.forEach((ksrlisting) => {
      reasoncodeoptions.push(<option  value={ksrlisting} key={ksrlisting}>{ksrlisting}</option>);
    });
    const showtickets = this.state.ticketoption
    let tickets = null;
    if (showtickets ==="No"){
      tickets =  <input type="text" name="Ticket" onChange={ this.handleChange }value = {this.props.currenttimesheetline.Ticket} />
    }else{
      //value={ksrlisting}
      var ticketoptions = [];
      this.state.tickets.forEach((ksrlisting) => {
      ticketoptions.push(<option value = {this.props.currenttimesheetline.Ticket}  key={ksrlisting}>{ksrlisting}</option>);
    });
  tickets = (
  <FormGroup controlId="formControlsSelect">
    <ControlLabel></ControlLabel>
    <FormControl
      componentClass="select"
      placeholder="select"
      onChange={ this.handleChange }
      name="Ticket"
    >
    {ticketoptions}
    </FormControl>
  </FormGroup> )

}



    return (
      <div>
      <Table responsive bordered condensed hover>
  <thead>
    <tr>

      <th>KSR/TA #</th>
      <th>Ticket/SOR Number</th>
      <th>Reason Code</th>
      <th>Description</th>

    </tr>
  </thead>
  <tbody>
  <tr>

    <td>
    <FormGroup controlId="formControlsSelect">
      <ControlLabel></ControlLabel>
      <FormControl
        componentClass="select"
        placeholder="select"
        onChange={ this.handleChange }
        name="KSR"
        value = {this.props.currenttimesheetline.KSR}
      >
      {ksroptions}
      </FormControl>
    </FormGroup>
   </td>
    <td>
      {tickets}
    </td>
    <td>
    <FormGroup controlId="formControlsSelect">
      <ControlLabel></ControlLabel>
      <FormControl
        componentClass="select"
        placeholder="select"
        onChange={ this.handleChange }
        name="ReasonCode"

      >
      {reasoncodeoptions}
      </FormControl>
    </FormGroup>
    </td>
    <td><input type="text" name="Description" onChange={ this.handleChange } value = {this.props.currenttimesheetline.Description} /></td>


    </tr>


          </tbody>
    </Table>

    <Table responsive bordered condensed hover>
<thead>
  <tr>


    <th>Sat</th>
    <th>Sun</th>
    <th>Mon</th>
    <th>Tue</th>
    <th>Wed</th>
    <th>Thu</th>
    <th>Fri</th>

  </tr>
</thead>
<tbody>
<tr>


  <td><input type="text" name="Sat" size="1" onChange={ this.handleChange } value = {this.props.currenttimesheetline.Sat}/></td>
  <td><input type="text" name="Sun" size="1" onChange={ this.handleChange }  value = {this.props.currenttimesheetline.Sun}/></td>
  <td><input type="text" name="Mon" size="1" onChange={ this.handleChange } value = {this.props.currenttimesheetline.Mon}  /></td>
  <td><input type="text" name="Tue" size="1" onChange={ this.handleChange } value = {this.props.currenttimesheetline.Tue}  /></td>
  <td><input type="text" name="Wed" size="1" onChange={ this.handleChange } value = {this.props.currenttimesheetline.Wed}  /></td>
  <td><input type="text" name="Thu" size="1" onChange={ this.handleChange } value = {this.props.currenttimesheetline.Thu} /></td>
  <td><input type="text" name="Fri" size="1" onChange={ this.handleChange } value = {this.props.currenttimesheetline.Fri} /></td>


  </tr>


        </tbody>
  </Table>


</div>
      );
  }
}
