export default function reducer(state={
    timesheetlines: [],
    blanktimesheet:  {
      "UserName" : "",
      "WeekEnding" : "",
      "id":"" ,
      "text":  ""  ,
      "complete":  ""  ,
      "KSR": "" ,
      "Ticket":  "" ,
      "ReasonCode":  ""  ,
      "Description":  ""  ,
      "Sat": "0"  ,
      "Sun": "0"  ,
      "Mon":  "0"  ,
      "Tue":  "0"  ,
      "Wed":  "0"  ,
      "Thu":"0"  ,
      "Fri": "0"  ,
      "Total": "0"  ,
    },
    currenttimesheetline:  {
      "UserName" : "",
      "WeekEnding" : "",
      "id":"" ,
      "text":  ""  ,
      "complete":  ""  ,
      "KSR": "" ,
      "Ticket":  "" ,
      "ReasonCode":  ""  ,
      "Description":  ""  ,
      "Sat": "0"  ,
      "Sun": "0"  ,
      "Mon":  "0"  ,
      "Tue":  "0"  ,
      "Wed":  "0"  ,
      "Thu":"0"  ,
      "Fri": "0"  ,
      "Total": "0"  ,
    },
    timesheettotals:  {
      "UserName" : "",
      "WeekEnding" : "",
      "id":"" ,
      "text":  ""  ,
      "complete":  ""  ,
      "KSR": "" ,
      "Ticket":  "" ,
      "ReasonCode":  ""  ,
      "Description":  ""  ,
      "Sat": "0"  ,
      "Sun": "0"  ,
      "Mon":  "0"  ,
      "Tue":  "0"  ,
      "Wed":  "0"  ,
      "Thu":"0"  ,
      "Fri": "0"  ,
      "Total": "0"  ,
    },
    fetching: false,
    fetched: false,
    error: null,
    currentuser:"",
    currentdate:"",
    ksrlisting: [],
  }, action) {

    // console.log("Begin TimeSheet Reducer")
    // console.log(state)
      // console.log(action.type)
      // console.log(action.payload)
        // console.log("End TimeSheet Reducer")
    switch (action.type) {
      case "FETCH_TIMESHEETLINES": {
        return {...state, fetching: true}
      }

      case "FETCH_KSRLISTING_FULFILLED": {
        return {...state,
          ksrlisting: action.payload,
          }
      }

      case "FETCH_TIMESHEETLINES_REJECTED": {
        return {...state, fetching: false, error: action.payload}
      }

      case "FETCH_TIMESHEETLINES_FULFILLED": {

        var Sat = 0
        var Sun = 0
        var Mon=  0
        var Tue =  0
        var Wed =   0
        var Thu = 0
        var Fri =  0
        var Total=  0
        action.payload.forEach((timesheet) => {
         Sat =   Sat +parseFloat( timesheet.Sat)
        Sun =   Sun +parseFloat( timesheet.Sun)
        Mon =   Mon +parseFloat( timesheet.Mon)
        Tue =   Tue +parseFloat( timesheet.Tue)
        Wed =   Wed +parseFloat( timesheet.Wed)
        Thu =   Thu +parseFloat( timesheet.Thu)
        Fri =   Fri +parseFloat( timesheet.Fri)
        Total =   Total +parseFloat( timesheet.Total)
        });
        const totals = {
        Sat,
        Sun,
        Mon,
        Tue,
         Wed,
         Thu,
         Fri,
         Total,
       }
       console.log("FETCH_TIMESHEETLINES_FULFILLED Reducer")
       console.log(totals)
        return {
          ...state,
          fetching: false,
          fetched: true,
          timesheetlines: action.payload,
            timesheettotals: totals,
        }
        //
      }

      case "SUBMIT_TIMESHEETLINES_REJECTED": {
        return {...state, fetching: false, error: action.payload}
      }

      case "SUBMIT_TIMESHEETLINES_FULFILLED": {
        return {
          ...state,
          fetching: false,
          fetched: true,

        }
        //timesheetlines: action.payload,
      }

      case "BLANK_TIMESHEETLINE": {
        console.log(action.payload)
        console.log(state)
        console.log(action.type)
        return {
          ...state,
          blanktimesheet: "action.payload",
          currenttimesheetline:  action.payload,

        }
      }

      case "ADD_TIMESHEETLINE": {
        var Sat = 0
        var Sun = 0
        var Mon=  0
        var Tue =  0
        var Wed =   0
        var Thu = 0
        var Fri =  0
        var Total=  0
        state.timesheetlines.forEach((timesheet) => {
         Sat =   Sat +parseFloat( timesheet.Sat)
        Sun =   Sun +parseFloat( timesheet.Sun)
        Mon =   Mon +parseFloat( timesheet.Mon)
        Tue =   Tue +parseFloat( timesheet.Tue)
        Wed =   Wed +parseFloat( timesheet.Wed)
        Thu =   Thu +parseFloat( timesheet.Thu)
        Fri =   Fri +parseFloat( timesheet.Fri)
        Total =   Total +parseFloat( timesheet.Total)
        });


         Sat =   Sat +parseFloat( action.payload.Sat)
        Sun =   Sun +parseFloat( action.payload.Sun)
        Mon =   Mon +parseFloat( action.payload.Mon)
        Tue =   Tue +parseFloat( action.payload.Tue)
        Wed =   Wed +parseFloat( action.payload.Wed)
        Thu =   Thu +parseFloat( action.payload.Thu)
        Fri =   Fri +parseFloat( action.payload.Fri)
        Total =   Total +parseFloat( action.payload.Total)

        const totals = {
        Sat,
        Sun,
        Mon,
        Tue,
        Wed,
        Thu,
        Fri,
        Total,
        }
        console.log("ADD_TIMESHEETLINES Reducer")
        console.log(totals)


        return {
          ...state,
          timesheetlines: [...state.timesheetlines, action.payload],
          timesheettotals: totals,
        }
      }

      case "UPDATE_TIMESHEETTOTALS": {
        return {
          ...state,
          timesheettotals: action.payload,
        }
      }

      case "UPDATE_TIMESHEETINFODATE": {
        return {
          ...state,
          currentdate: action.payload,
        }
      }

      case "UPDATE_TIMESHEETINFOUSERNAME": {
        return {
          ...state,
          currentuser: action.payload,
        }
      }

      case "UPDATE_TIMESHEETLINE": {
          console.log("XXXXXXXXXXXXXXXXXXXXUPDATE_TIMESHEETLINE reducer")
            console.log(action.payload)
        const { id, text } = action.payload
          console.log(id)
          console.log(text)
            console.log(action.payload)
        const newtimesheetlines = [...state.timesheetlines]
        const timesheetlineToUpdate = newtimesheetlines.findIndex(timesheetline => timesheetline.id === id)
        newtimesheetlines[timesheetlineToUpdate] = action.payload;

          console.log("XXXXXXXXXXXXXXXXXXXUPDATE_TIMESHEETLINE reducer END")
        //timesheetlines: newtimesheetlines,
        return {
          ...state,
          timesheetlines: newtimesheetlines,

        }
      }

      case "GET_TIMESHEETLINE": {

        const { id } = action.payload
        const newtimesheetlines = [...state.timesheetlines]
        const timesheetlineToUpdate = newtimesheetlines.findIndex(timesheetline => timesheetline.id === id)
          console.log("GET_TIMESHEETLINE" + newtimesheetlines[0])
          console.log( timesheetlineToUpdate)
        //    console.log("GET_TIMESHEETLINE" + state.timesheetlines[1])
        console.log("GET_TIMESHEETLINE2" + newtimesheetlines[timesheetlineToUpdate])
        return {
          ...state,
          currenttimesheetline: newtimesheetlines[timesheetlineToUpdate]
        }
      }


      case "DELETE_TIMESHEETLINE": {
        return {
          ...state,
          timesheetlines: state.timesheetlines.filter(timesheetline => timesheetline.id !== action.payload),
        }
      }
    }

    return state
}
