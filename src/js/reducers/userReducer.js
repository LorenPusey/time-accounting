export default function reducer(state={
    userinfo: {
      Status: null,
      UserFullName: null,
      UserCommonName: null,
    },
    fetching: false,
    fetched: false,
    error: null,
    
  }, action) {

  // console.log("Begin fetchUser Reducer")
  // console.log(state)
    // console.log(action.type)
    // console.log(action.payload)
      // console.log("End fetchUser Reducer")
    switch (action.type) {
      case "FETCH_USERINFO": {
        return {...state, fetching: true}
      }
      case "FETCH_USERINFO_REJECTED": {
        console.log(action.payload)
        return {...state, fetching: false,fetched: false, error: action.payload}
      }
      case "FETCH_USERINFO_FULFILLED": {
        // console.log("Begin fetchUser Reducer2")
          // console.log(action.type)
          // console.log(action.payload)
            // console.log("End fetchUser Reduce2r")
        return {
          ...state,
          fetching: false,
          fetched: true,
          userinfo: action.payload,
        }
      }
      case "SET_USER_NAME": {
        return {
          ...state,
          user: {...state.user, name: action.payload},
        }
      }
      case "SET_USER_AGE": {
        return {
          ...state,
          user: {...state.user, age: action.payload},
        }
      }
    }

    return state
}
