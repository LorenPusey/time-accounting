import { combineReducers } from "redux"

import timesheet from "./timesheetReducer"
import user from "./userReducer"

export default combineReducers({
  timesheet,
  user,
})
