import axios from "axios";
import buffer from "Buffer"

export function fetchTimesheetlines(date, username) {

  /*
  return {
    type: "FETCH_TIMESHEETLINES_FULFILLED",
    payload:  [{
       "id": "412946475-2",
       "text": "Loren Pusey",
       "complete": true,
       "KSR": "17001-BG",
       "Ticket": "284888122-4",
       "ReasonCode": "cursus vestibulum proin eu",
       "Description": "eget tincidunt eget tempus vel pede morbi porttitor",
       "Sat": 2,
       "Sun": 7,
       "Mon": 3,
       "Tue": 2,
       "Wed": 5,
       "Thu": 1,
       "Fri": 6,
       "Total": 3
     }, {
       "id": "566435879-3",
       "text": "Loren Pusey",
       "complete": false,
       "KSR": "Z-Vacation",
       "Ticket": "393384581-5",
       "ReasonCode": "ut ultrices vel augue vestibulum",
       "Description": "nam nulla integer pede justo",
       "Sat": 6,
       "Sun": 6,
       "Mon": 1,
       "Tue": 7,
       "Wed": 6,
       "Thu": 7,
       "Fri": 6,
       "Total": 5
     },]

   }
   */

   return function(dispatch) {
  //  axios.get("http://rest.learncode.academy/api/test123/tweets")
    console.log("FETCH_TIMESHEETLINES_FULFILLEDXXXX");
    var string = 'CN=Loren Pusey/O=kubota'
    var encodeName = btoa(username)
    console.log(encodeName + username)
    var string = '04/07/2017'
    var encodeDate = btoa(date)
    console.log(encodeDate +date)
    var url = "http://corp1.kubota.com/Development/KTAWeb.nsf/TimeSheet.xsp" +
              '?Date="' +  encodeDate +
              '"&UserName="' + encodeName +"\""
              console.log(url)
              //?Date="MDQvMDcvMjAxNw=="&UserName="Q049TG9yZW4gUHVzZXkvTz1rdWJvdGE="
              //var Buffer = require('buffer/').Buffer
              //    var b = new Buffer('JavaScript');
              //    var s = b.toString('base64')
              //    console.log(b)
              //  axios.get("http://corp1.kubota.com/Development/KTAWeb.nsf/TimeSheet.xsp")
      axios.get(url)
      .then((response) => {
          console.log("FETCH_TIMESHEETLINES_FULFILLED");
            console.log( response.data);
        dispatch({type: "FETCH_TIMESHEETLINES_FULFILLED", payload: response.data})
      })
      .catch((err) => {
          console.log("FETCH_TIMESHEETLINES_REJECTED");
        dispatch({type: "FETCH_TIMESHEETLINES_REJECTED", payload: err})
      })
  }

}

export function fetchKSRListing() {
  return function(dispatch) {



   var url = "http://corp1.kubota.com/Development/KTAWeb.nsf/TimeSheet.xsp?Request=GetReasonCodes"
             console.log(url)

     axios.get(url)
     .then((response) => {
         console.log("FETCH_KSRLISTING_FULFILLED");
           console.log( response.data);
       dispatch({type: "FETCH_KSRLISTING_FULFILLED", payload: response.data})
     })
     .catch((err) => {
         console.log("FETCH_KSRLISTING_REJECTED");
       dispatch({type: "FETCH_KSRLISTING_REJECTED", payload: err})
     })
 }
  }

export function submitTimesheetlines(timesheets, date, username) {

  return function(dispatch) {
 //  axios.get("http://rest.learncode.academy/api/test123/tweets")
 console.log("SUBMIT_TIMESHEETLINES_FULFILLEDXXXX" + timesheets);
 console.log(timesheets);

      //var url = "http://corp1.kubota.com/Development/KTAWeb.nsf/TimeSheet.xsp"// +
      //   '?Date="' +  encodeDate +
      //       '"&UserName="' + encodeName +"\""

      var encodeName = btoa(username.UserName)
      console.log(encodeName + username.UserName)
      var string = '04/07/2017'
      var encodeDate = btoa(date.Date)
        console.log(encodeDate +date.Date)
      var url = "http://corp1.kubota.com/Development/KTAWeb.nsf/TimeSheet.xsp" +
                  '?Date="' +  encodeDate +
                  '"&UserName="' + encodeName +"\""


             console.log(url)
 //?Date="MDQvMDcvMjAxNw=="&UserName="Q049TG9yZW4gUHVzZXkvTz1rdWJvdGE="
 //var Buffer = require('buffer/').Buffer
 //    var b = new Buffer('JavaScript');
 //    var s = b.toString('base64')
 //    console.log(b)
 //  axios.get("http://corp1.kubota.com/Development/KTAWeb.nsf/TimeSheet.xsp")
 //Basic bHB1c2V5OkxMMjExQGt1YjdkZjg0Mw==
    axios.defaults.headers.common['Authorization'] = "Basic bHB1c2V5OkxMMjExQGt1YjdkZjg0Mw==";
    //axios.defaults.headers.common['ContentType'] = "text/plain";
     axios.post(url, timesheets)
     .then((response) => {
         console.log("SUBMIT_TIMESHEETLINES_FULFILLED");
           console.log( response.data);

       dispatch({type: "SUBMIT_TIMESHEETLINES_FULFILLED", payload: response.data})
     })
     .catch((err) => {
         console.log("SUBMIT_TIMESHEETLINES_REJECTED");
       dispatch({type: "SUBMIT_TIMESHEETLINES_REJECTED", payload: err})
     })
 }

}

export function getTimesheetline(id ) {
  return {
    type: 'GET_TIMESHEETLINE',
    payload: {
      id,
    },

  }

}

export function addTimesheetline(id,  text, date, username ,timesheets) {

    console.log("addTimesheetlineX");
    console.log(text);
    console.log(date);
    console.log(username);
    console.log(date.Date);
    console.log(username.UserName);

    console.log("addTimesheetlinexxxxxxxxx");
    var Sat = parseFloat( text.Sat)
    var Sun = parseFloat( text.Sun)
    var Mon=  parseFloat( text.Mon)
    var Tue =  parseFloat( text.Tue)
    var Wed =   parseFloat( text.Wed)
    var Thu = parseFloat( text.Thu)
    var Fri =  parseFloat( text.Fri)
    var Total=  parseFloat( text.Total)
    timesheets.forEach((timesheet) => {
      Sat =   Sat +parseFloat( timesheet.Sat)
      Sun =   Sun +parseFloat( timesheet.Sun)
      Mon =   Mon +parseFloat( timesheet.Mon)
      Tue =   Tue +parseFloat( timesheet.Tue)
      Wed =   Wed +parseFloat( timesheet.Wed)
      Thu =   Thu +parseFloat( timesheet.Thu)
      Fri =   Fri +parseFloat( timesheet.Fri)
      Total =   Total +parseFloat( timesheet.Total)
    });


  //  tempsheets.push(text)

  return {
    type: 'ADD_TIMESHEETLINE',
    payload: {
      "UserName" : username.UserName,
      "WeekEnding" : date.Date,
      "id": id.toString()  ,
      "text":  text.text  ,
      "complete":  text.complete  ,
      "KSR": text.KSR  ,
      "Ticket":  text.Ticket  ,
      "ReasonCode":  text.ReasonCode  ,
      "Description":  text.Description  ,
      "Sat": text.Sat  ,
      "Sun": text.Sun  ,
      "Mon":  text.Mon  ,
      "Tue":  text.Tue  ,
      "Wed":  text.Wed  ,
      "Thu": text.Thu  ,
      "Fri": text.Fri  ,
      "Total":  text.Total  ,
    },

  }
}

export function getBlankTimesheetline() {
    const id = Date.now();
  return {
    type: 'BLANK_TIMESHEETLINE',
    payload: {
      "id": id.toString()  ,
      "text":  ""  ,
      "complete":  ""  ,
      "KSR": "" ,
      "Ticket":  "" ,
      "ReasonCode":  ""  ,
      "Description":  ""  ,
      "Sat": "0"  ,
      "Sun": "0"  ,
      "Mon":  "0"  ,
      "Tue":  "0"  ,
      "Wed":  "0"  ,
      "Thu":"0"  ,
      "Fri": "0"  ,
      "Total": "0"  ,
    },
  }
}

export function updateTimesheetTotals(timesheets, date, username) {

    console.log("updateTimesheetTotals");
    console.log(timesheets);
  var Sat = 0
  var Sun = 0
  var Mon=  0
  var Tue =  0
  var Wed =   0
  var Thu = 0
  var Fri =  0
  var Total=  0
  timesheets.forEach((timesheet) => {
   Sat =   Sat +parseFloat( timesheet.Sat)
  Sun =   Sun +parseFloat( timesheet.Sun)
  Mon =   Mon +parseFloat( timesheet.Mon)
  Tue =   Tue +parseFloat( timesheet.Tue)
  Wed =   Wed +parseFloat( timesheet.Wed)
  Thu =   Thu +parseFloat( timesheet.Thu)
  Fri =   Fri +parseFloat( timesheet.Fri)
  Total =   Total +parseFloat( timesheet.Total)
  });
  return {
    type: 'UPDATE_TIMESHEETTOTALS',
    payload: {
      Sat,
      Sun,
     Mon,
     Tue,
     Wed,
     Thu,
     Fri,
     Total,
    },
  }
}

export function updateTimesheetline(id, text2) {

    console.log("XXXXXXXXXXXXXXXXXXXXXXXXTimesheet action update text" + text2)
    var temptext = text2
    console.log(temptext)
      console.log(temptext.Total)
    console.log(temptext['Total'])
  //"id": "412946475-2",
  var UserName = temptext.UserName
  var WeekEnding = temptext.WeekEnding
  var text = temptext.text
  var complete = temptext.complete
  var KSR = temptext['KSR']
  var Ticket = temptext.Ticket
  var ReasonCode = temptext.ReasonCode
  var Description = temptext.Description
  var Sat = temptext.Sat
  var Sun = temptext.Sun
  var Mon = temptext.Mon
  var Tue = temptext.Tue
  var Wed = temptext.Wed
  var Thu = temptext.Thu
  var Fri = temptext.Fri
  var Total = temptext.Total
  console.log("Timesheet action update Total" +Total)
  console.log("Timesheet action update Total" + KSR )

  return {
    type: 'UPDATE_TIMESHEETLINE',
    payload: {
      id,
      UserName,
      WeekEnding,
      text,
      complete,
      KSR,
      Ticket,
      ReasonCode,
      Description,
      Sat,
      Sun,
      Mon,
      Tue,
      Wed,
      Thu,
      Fri,
      Total,
    },
  }
}

export function updateTimesheetlineItem(id, text) {

  return {
    type: 'UPDATE_TIMESHEETLINE_ITEM',
    payload: {
      id,
      text,

    },
  }
}

export function updateTimesheetInfoDate(Date) {
  return {
    type: 'UPDATE_TIMESHEETINFODATE',
    payload: {
      Date,
    },
  }
}

export function updateTimesheetInfoUserName( UserName) {
  return {
    type: 'UPDATE_TIMESHEETINFOUSERNAME',
    payload: {
      UserName,
    },
  }
}

export function deleteTimesheetline(id) {
  return { type: 'DELETE_TIMESHEETLINE', payload: id}
}
