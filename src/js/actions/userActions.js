import axios from "axios";


export function fetchUserInfo() {
    console.log("Begin fetchUser")

    return function(dispatch) {
   //  axios.get("http://rest.learncode.academy/api/test123/tweets")

   var url = "http://corp1.kubota.com/Development/KTAWeb.nsf/TimeSheet.xsp?Request=GetUser"

console.log(url)
   //  axios.get("http://corp1.kubota.com/Development/KTAWeb.nsf/TimeSheet.xsp")
       axios.get(url)
       .then((response) => {
           console.log("FETCH_USERINFO_FULFILLED");
             console.log( response.data);
         dispatch({type: "FETCH_USERINFO_FULFILLED", payload: response.data})
       })
       .catch((err) => {
           console.log("FETCH_USERINFO_REJECTED");
         dispatch({type: "FETCH_USERINFO_REJECTED", payload: err})
       })
   }

 }


export function setUserName(name) {
  return {
    type: 'SET_USER_NAME',
    payload: name,
  }
}

export function setUserAge(age) {
  return {
    type: 'SET_USER_AGE',
    payload: age,
  }
}
